package com.example.memorygame

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.widget.Button

import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class ResultScreen : AppCompatActivity() {

    private lateinit var playbutton: Button
    private lateinit var menubutton : Button
    private lateinit var resultTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.result_screen)



        playbutton = findViewById(R.id.play_button)
        menubutton = findViewById(R.id.menu_button)
        resultTextView = findViewById(R.id.resultTextView)

        val valorSpinner = intent.extras!!.getString("dificulty")

        val bundle: Bundle? = intent.extras

        resultTextView.text = bundle?.getString(RESULT_KEY)

        playbutton.setOnClickListener {
            if (valorSpinner == "Easy") {
                val intent1 = Intent(this, GameActivity::class.java)
                intent.putExtra("dificulty", valorSpinner)
                startActivity(intent1)
            } else if (valorSpinner == "Hard") {
                val intent2 = Intent(this, GameActivityHard::class.java)
                intent.putExtra("dificulty", valorSpinner)
                startActivity(intent2)
            }
        }

        menubutton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}
