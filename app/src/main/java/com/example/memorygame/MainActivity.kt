package com.example.memorygame

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder

const val spiner_value = "value"

class MainActivity : AppCompatActivity() {

    private lateinit var playbutton: Button
    private lateinit var helpbutton: Button


    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.drawable.splash_background)
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_main)

        playbutton = findViewById(R.id.play_button)
        helpbutton = findViewById(R.id.help_button)

        val spinner: Spinner = findViewById(R.id.dificult)

        ArrayAdapter.createFromResource(
            this,
            R.array.Difficulty,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }

        playbutton.setOnClickListener {
            val valorSpinner = spinner.selectedItem.toString()
            if (valorSpinner == "Easy"){
                val intent = Intent(this, GameActivity::class.java)
                intent.putExtra("dificulty", valorSpinner)
                startActivity(intent)
            } else if (valorSpinner == "Hard"){
                val intent =  Intent(this, GameActivityHard::class.java)
                intent.putExtra("dificulty", valorSpinner)
                startActivity(intent)
            }
        }

        helpbutton.setOnClickListener {
            MaterialAlertDialogBuilder(this)
                .setTitle(resources.getString(R.string.help))
                .setMessage(resources.getString(R.string.helpText))
                .setNegativeButton(resources.getString(R.string.close)) { dialog, which ->
                    closeContextMenu()
                }
                .show()
        }

    }
}