package com.example.memorygame

import androidx.lifecycle.ViewModel

class GameModelView : ViewModel() {
    // Game data model
    var imatges = arrayOf(
        R.drawable.card4,
        R.drawable.card2,
        R.drawable.card3,
        R.drawable.card4,
        R.drawable.card2,
        R.drawable.card3
    )
    var cartes = mutableListOf<Carta>()
    var movements = 0

    // Aquesta funció es estàndard de cada classe, i és com el constructor

    init {
        setDataModel()
    }

    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    fun setDataModel() {
        imatges.shuffle()
        for (i in 0..5) {
            cartes.add(Carta(i, imatges[i]))
        }
    }

    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar
    fun girarCarta(idCarta: Int) : Int {
        if (!cartes[idCarta].girada) {
            cartes[idCarta].girada = true
            return cartes[idCarta].resId
        } else {
            cartes[idCarta].girada = false
            return R.drawable.fondo_carta
        }
    }

    // Funció que posa l'estat del joc al mode inicial
    fun resetEstatJoc() {
        for (i in 0..5) {
            cartes[i].girada = false
            cartes[i].match = false
        }
    }

    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        if(cartes[idCarta].girada) return cartes[idCarta].resId
        else return R.drawable.fondo_carta
    }
    fun final(): Boolean {
        for (i in 0..5) {
            if (!cartes[i].girada) return false
        }
        return true
    }
}