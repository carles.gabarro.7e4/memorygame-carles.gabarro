package com.example.memorygame


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

const val RESULT_KEY = "RESULT"

class GameActivity : AppCompatActivity(), View.OnClickListener{

    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView

    private lateinit var resetButton: Button

    private lateinit var textview: TextView

    private lateinit var viewModel: GameModelView

    private lateinit var array: Array<ImageView>

    private var movement = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.game_screen)



        viewModel = ViewModelProvider(this).get(GameModelView::class.java)

        carta1 = findViewById(R.id.imageButton1); carta2 = findViewById(R.id.imageButton2)
        carta3 = findViewById(R.id.imageButton3); carta4 = findViewById(R.id.imageButton4)
        carta5 = findViewById(R.id.imageButton5); carta6 = findViewById(R.id.imageButton6)

        resetButton = findViewById(R.id.resetButton)

        textview = findViewById(R.id.textView2G1)

        array = arrayOf(carta1, carta2, carta3, carta4, carta5, carta6)


        carta1.setOnClickListener(this); carta2.setOnClickListener(this)
        carta3.setOnClickListener(this); carta4.setOnClickListener(this)
        carta5.setOnClickListener(this); carta6.setOnClickListener(this)

        updateUI()

        resetButton.setOnClickListener{
            viewModel.resetEstatJoc()
        }

    }

    private fun updateUI() {
        carta1.setImageResource(viewModel.estatCarta(0)); carta2.setImageResource(viewModel.estatCarta(1))
        carta3.setImageResource(viewModel.estatCarta(2)); carta4.setImageResource(viewModel.estatCarta(3))
        carta5.setImageResource(viewModel.estatCarta(4)); carta6.setImageResource(viewModel.estatCarta(5))
    }

    override fun onClick(v: View?) {
        when (v) {
            carta1 -> girarCarta(0, carta1); carta2 -> girarCarta(1, carta2)
            carta3 -> girarCarta(2, carta3); carta4 -> girarCarta(3, carta4)
            carta5 -> girarCarta(4, carta5); carta6 -> girarCarta(5, carta6)
        }
        if (viewModel.final()) {
            val valorSpinner = intent.extras!!.getString("dificulty")
            val intent = Intent(this, ResultScreen::class.java)
            intent.putExtra("dificulty", valorSpinner)
            intent.putExtra(RESULT_KEY, calcularResultat().toString())
            startActivity(intent)
        }
    }
    @SuppressLint("SetTextI18n")
    private fun girarCarta(idCarta: Int, carta: ImageView) {
        carta.setImageResource(viewModel.girarCarta(idCarta))
        Handler(Looper.getMainLooper()).postDelayed({
            comprovar()
        }, 350)
        movement++
        textview.text = "Movements: $movement"
    }

    private fun comprovar(){
        var cartaGirada1: Carta? = null
        var cartaGirada2: Carta? = null
        for (carta in viewModel.cartes){
            if(carta.girada && !carta.match){
                if(cartaGirada1 == null){
                    cartaGirada1 = carta
                } else {
                    cartaGirada2 = carta
                }
            }
        }
        if (cartaGirada1 != null && cartaGirada2 != null){
            if(cartaGirada1.resId != cartaGirada2.resId){
                array[cartaGirada1.id].setImageResource(R.drawable.fondo_carta)
                array[cartaGirada2.id].setImageResource(R.drawable.fondo_carta)
                cartaGirada1.girada = false
                cartaGirada2.girada = false

            } else {
                cartaGirada1.match = true
                array[cartaGirada1.id].setOnClickListener(null)
                cartaGirada2.match = true
                array[cartaGirada2.id].setOnClickListener(null)
            }
        }
    }
    fun calcularResultat(): Int {
        return 100 - (movement-6)*10
    }

}
