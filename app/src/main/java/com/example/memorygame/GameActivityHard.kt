package com.example.memorygame

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider

const val RESULT_KEY_2 = "RESULT"

class GameActivityHard : AppCompatActivity(), View.OnClickListener{
    
    private lateinit var carta1: ImageView
    private lateinit var carta2: ImageView
    private lateinit var carta3: ImageView
    private lateinit var carta4: ImageView
    private lateinit var carta5: ImageView
    private lateinit var carta6: ImageView
    private lateinit var carta7: ImageView
    private lateinit var carta8: ImageView


    private lateinit var resetButton: Button
    private lateinit var textViewG2: TextView

    private lateinit var viewModel: GameModelViewHard

    private lateinit var array: Array<ImageView>

    private var movement = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.game_screen_hard)

        viewModel = ViewModelProvider(this).get(GameModelViewHard::class.java)

        carta1 = findViewById(R.id.imageView); carta2 = findViewById(R.id.imageView2)
        carta3 = findViewById(R.id.imageView3); carta4 = findViewById(R.id.imageView4)
        carta5 = findViewById(R.id.imageView5); carta6 = findViewById(R.id.imageView6)
        carta7 = findViewById(R.id.imageView7); carta8 = findViewById(R.id.imageView8)


        resetButton = findViewById(R.id.resetButton)

        textViewG2 = findViewById(R.id.textViewG2)

        array = arrayOf(carta1, carta2, carta3, carta4, carta5, carta6, carta7, carta8)

        carta1.setOnClickListener(this); carta2.setOnClickListener(this)
        carta3.setOnClickListener(this); carta4.setOnClickListener(this)
        carta5.setOnClickListener(this); carta6.setOnClickListener(this)
        carta7.setOnClickListener(this); carta8.setOnClickListener(this)

        updateUI()

        resetButton.setOnClickListener {
            viewModel.resetEstatJoc()
        }

    }

    private fun updateUI() {
        carta1.setImageResource(viewModel.estatCarta(0)); carta2.setImageResource(viewModel.estatCarta(1))
        carta3.setImageResource(viewModel.estatCarta(2)); carta4.setImageResource(viewModel.estatCarta(3))
        carta5.setImageResource(viewModel.estatCarta(4)); carta6.setImageResource(viewModel.estatCarta(5))
        carta7.setImageResource(viewModel.estatCarta(6)); carta8.setImageResource(viewModel.estatCarta(7))

    }

    override fun onClick(v: View?) {
        when (v) {
            carta1 -> girarCarta(0, carta1); carta2 -> girarCarta(1, carta2)
            carta3 -> girarCarta(2, carta3); carta4 -> girarCarta(3, carta4)
            carta5 -> girarCarta(4, carta5); carta6 -> girarCarta(5, carta6)
            carta7 -> girarCarta(6, carta7); carta8 -> girarCarta(7, carta8)
        }
        if (viewModel.final()) {
            val valorSpinner = intent.extras!!.getString("dificulty")
            val intent = Intent(this, ResultScreen::class.java)
            intent.putExtra("dificulty", valorSpinner)
            intent.putExtra(RESULT_KEY_2, calcularResultat().toString())
            startActivity(intent)
        }
    }
    @SuppressLint("SetTextI18n")
    private fun girarCarta(idCarta: Int, carta: ImageView) {
        carta.setImageResource(viewModel.girarCarta(idCarta))
        Handler(Looper.getMainLooper()).postDelayed({
            comprovar()
        }, 350)
        movement++
        textViewG2.text = "Movements: " + (movement + 1)
    }

    private fun comprovar() {
        var cartaGirada1: Carta? = null
        var cartaGirada2: Carta? = null

        for (carta in viewModel.cartes2) {
            if (carta.girada && !carta.match) {
                if (cartaGirada1 == null) {
                    cartaGirada1 = carta
                    array[cartaGirada1.id].setOnClickListener(null)
                } else if (cartaGirada2 == null){
                    cartaGirada2 = carta
                    array[cartaGirada2.id].setOnClickListener(null)
                }
            }
            if (cartaGirada1 != null && cartaGirada2 != null) {
                if (cartaGirada1.resId != cartaGirada2.resId) {
                    array[cartaGirada1.id].setOnClickListener(this)
                    array[cartaGirada2.id].setOnClickListener(this)
                    array[cartaGirada1.id].setImageResource(R.drawable.fondo_carta)
                    array[cartaGirada2.id].setImageResource(R.drawable.fondo_carta)

                    cartaGirada1.girada = false
                    cartaGirada2.girada = false


                } else {
                    cartaGirada1.match = true
                    array[cartaGirada1.id].setOnClickListener(null)
                    cartaGirada2.match = true
                    array[cartaGirada2.id].setOnClickListener(null)
                }
            }
        }
    }
    fun calcularResultat(): Int {
        return 100 - (movement-8)*10
    }
}