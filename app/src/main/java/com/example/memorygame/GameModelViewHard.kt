package com.example.memorygame


import androidx.lifecycle.ViewModel


class GameModelViewHard : ViewModel() {
    // Game data model
    var imatges2 = arrayOf(
        R.drawable.card1,
        R.drawable.card2,
        R.drawable.card3,
        R.drawable.card4,
        R.drawable.card1,
        R.drawable.card2,
        R.drawable.card3,
        R.drawable.card4
    )
    var cartes2 = mutableListOf<Carta>()


    // Aquesta funció es estàndard de cada classe, i és com el constructor

    init {
        setDataModel()
    }

    // Funció que barreja les imatges de les cartes i crea l'array de Cartes
    private fun setDataModel() {
        imatges2.shuffle()
        for (i in 0..7) {
            cartes2.add(Carta(i, imatges2[i]))
        }
    }

    // Funció que comprova l'estat de la carta, el canvia i envia
    // a la vista la imatge que s'ha de pintar
    fun girarCarta(idCarta: Int) : Int {
        if (!cartes2[idCarta].girada) {
            cartes2[idCarta].girada = true
            return cartes2[idCarta].resId
        } else {
            cartes2[idCarta].girada = false
            return R.drawable.fondo_carta
        }
    }

    // Funció que posa l'estat del joc al mode inicial
    fun resetEstatJoc() {
        for (i in 0..7) {
            cartes2[i].girada = false
            cartes2[i].match = false
        }
    }

    // Funció que retorna l'estat actual d'una carta
    fun estatCarta(idCarta: Int): Int {
        if(cartes2[idCarta].girada) return cartes2[idCarta].resId
        else return R.drawable.fondo_carta
    }
    fun final(): Boolean {
        for (i in 0..5) {
            if (!cartes2[i].girada) return false
        }
        return true
    }
    }
